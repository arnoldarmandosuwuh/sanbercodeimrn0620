import { StatusBar } from 'expo-status-bar'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Youtube from './Tugas/Tugas12/App'
import Todo from './Tugas/Tugas14/App'
import Screen from './Tugas/Tugas15/index'
import Index from './Tugas/TugasNavigation/index'
import Quiz from './Tugas/Quiz3/index'

const App = () => {
  return (
    <Quiz />
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default App