import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillComponent extends Component {
    render = () => {
        let skill = this.props.skill
        return (
            <View style={styles.body}>
                <View style={styles.logo2}>
                    <Icon name={skill.iconName} size={70} color="#003366" />
                </View>
                <View style={styles.content}>
                    <Text style={styles.nameSkill}>{skill.skillName}</Text>
                    <Text style={styles.tab}>{skill.categoryName}</Text>
                    <Text style={styles.presentage}>{skill.percentageProgress}</Text>
                </View>
                <TouchableOpacity>
                    <View style={styles.backRight}>
                        <Icon name="chevron-right" size={70} color="#003366" />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25
    },
    logo: {
        alignItems: "flex-end",
    },
    descName: {
        flexDirection: 'row',
        paddingHorizontal: 19,
        alignItems: 'center'
    },
    titleSkill: {
        paddingHorizontal: 19,
        marginBottom: 10
    },
    tabSkill: {
        flexDirection: 'row',
        paddingHorizontal: 19,
 
    },
    tabTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#003366',
        marginRight: 6,
        padding: 5,
        borderRadius: 8,
        backgroundColor: '#B4E9FF',
    },
    containerBody: {
        height: 129,
        marginHorizontal: 15,
        borderRadius: 8,
        backgroundColor: '#B4E9FF',
        marginTop: 10,
    },
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 12,
        paddingHorizontal: 10
    },
    nameSkill: {
        fontSize: 24,
        color: '#003366',
    },
    tab: {
        fontSize: 16,
        color: '#3EC6FF'
    },
    presentage: {
        fontSize: 48,
        color: '#fff',
        textAlign: 'right'
    }
})