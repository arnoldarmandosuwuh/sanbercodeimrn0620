import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    FlatList,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const DATA = [
    {
      id: '1',
      title: 'arnoldarmando07',
      type: 'facebook-box',
    },
    {
      id: '2',
      title: '@arnoldarmandosuwuh',
      type: 'instagram',
    },
    {
      id: '3',
      title: '@arnoldarmando07',
      type: 'twitter',
    },
];
const DATA2 = [
    {
      id: '1',
      title: '@arnoldarmandosuwuh',
      type: 'gitlab',
    },
    {
      id: '2',
      title: '@arnoldarmandosuwuh',
      type: 'github-circle',
    },
  ];

function Item({ id, title, type}) {
    return (
      <TouchableOpacity style={styles.item}>
        <Icon name={type} size={40}/>
        <Text style={styles.titleList}>{title}</Text>
      </TouchableOpacity>
    );
}
const ItemHorizontal = ({ id, title, type}) => {
    return (
      <TouchableOpacity style={styles.itemHorizontal}>
        <Icon name={type} size={40}/>
        <Text style={styles.titleList}>{title}</Text>
      </TouchableOpacity>
    );
  }
  

export default class About extends Component {
    render = () => {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('../Tugas13/assets/image/logo.png')} style={{ width: 98, height: 30 }}/>
                    <View style={styles.rightNav}>                    
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name="account-circle" size={25} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.titlePage}>ABOUT</Text>
                    <Image source={require('../Tugas13/assets/image/foto.jpg')} style={styles.circleImageLayout}/>
                    <Text style={styles.name}>Arnold Armando Suwuh</Text>
                    <Text style={styles.job}>Android Developer</Text>
                    <View style={styles.about}>
                        <Text style={styles.titlePanel}>Portofolio</Text>
                        <View style={styles.line}/>
                        <FlatList 
                            horizontal
                            data={DATA2} 
                            renderItem={({ item }) => (
                                <ItemHorizontal id={item.id} title={item.title} type={item.type}/>
                            )}
                            keyExtractor={(item) => item.id}
                            style={{width: "100%"}}
                        />
                    </View>
                    <View style={styles.contact}>
                        <Text style={styles.titlePanel}>Hubungi Saya</Text>
                        <View style={styles.line}/>
                        <FlatList 
                            data={DATA} 
                            renderItem={({ item }) => (
                                <Item id={item.id} title={item.title} type={item.type}/>
                            )}
                            keyExtractor={(item) => item.id}
                            style={{width: "100%"}}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    navBar: {
        height: 50,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rightNav: {
        flexDirection: 'row'
    },
    navItem: {
        marginLeft: 25
    },
    body: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titlePage: {
        fontWeight:"bold",
        fontSize:30,
        color:"#000000",
        marginBottom:20
    },
    circleImageLayout: {
        width: 150,
        height: 150,
        borderRadius: 150 / 2,
        marginBottom: 10
    },
    name: {
        fontWeight:"bold",
        fontSize:20,
        color:"#000000",
        marginBottom:10
    },
    job: {
        fontWeight: "bold",
        fontSize: 15,
        color: "#3EC6FF",
        marginBottom:10
    },
    item: {
        padding: 0,
        marginVertical: 5,
        marginHorizontal: 80,
        alignItems: "center",
        flexDirection:"row", 
    },
    itemHorizontal: {
        padding: 0,
        marginVertical: 5,
        marginHorizontal: 5,
        alignItems: "center",
        flexDirection:"column", 
    },
    titleList: {
        fontSize: 15,
        marginStart: 10
    },
    titlePanel: {
        fontSize: 20,
        marginHorizontal: 10,
        marginVertical: 10
    },
    line: {
        borderWidth: 1,
        marginHorizontal: 10,
        borderColor: "#000000"
    },
    about: {
        flex: 1,
        backgroundColor: "#EFEFEF",
        borderRadius: 25,
        marginVertical: 5,
        marginHorizontal: 10,
    },
    contact: {
        flex: 1,
        backgroundColor: "#EFEFEF",
        borderRadius: 25,
        marginVertical: 5,
        marginHorizontal: 10,
    },
})