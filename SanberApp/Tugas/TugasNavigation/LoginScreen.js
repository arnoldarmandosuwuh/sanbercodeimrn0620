import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native'

export const Login = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image source={require('../Tugas13/assets/image/logo.png')} resizeMode="contain" style={styles.brandImage}/>
            <Text style={styles.logo}>LOGIN</Text>
            <View style={styles.inputView}>
                <TextInput style={styles.inputText} placeholder="Username / Email" placeholderTextColor="#fff"/>
            </View>
            <View style={styles.inputView}>
                <TextInput secureTextEntry style={styles.inputText} placeholder="Password" placeholderTextColor="#fff"/>
            </View>
            <TouchableOpacity>
                <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styles.loginBtn}
                onPress={() => {navigation.navigate("Drawer")}} >
                <Text style = {styles.buttonText}> LOGIN </Text>
            </TouchableOpacity>
            <Text style={styles.orText}>OR</Text>
            <TouchableOpacity style={styles.registerBtn} 
                onPress={() => {
                    navigation.navigate("Register")
                }}>
                <Text style = {styles.buttonText}> REGISTER </Text>
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    brandImage: {
        marginBottom:20
    },
    logo:{
        fontWeight:"bold",
        fontSize:30,
        color:"#000000",
        marginBottom:20
    },
    inputView: {
        width: "80%",
        backgroundColor: "#00000050",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding:20
    },
    inputText:{
        height: 50,
        color: "white"
    },
    forgot:{
        color:"#000000",
        fontSize: 12,
        marginBottom: 20
    },
    orText:{
        color:"#000000",
        fontSize: 15,
        marginTop: 10,
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#3EC6FF",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
    },
    registerBtn: {
        width: "80%",
        backgroundColor: "#003366",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
    },
    buttonText:{
        color:"white"
    }
})