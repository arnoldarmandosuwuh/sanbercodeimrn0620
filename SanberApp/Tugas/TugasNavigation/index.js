import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { Login } from "./LoginScreen"
import { Register } from "./RegisterScreen"
import About from "./AboutScreen"
import Skill from "./SkillScreen"
import { Project } from "./ProjectScreen"
import { Add } from "./AddScreen"

const LoginStack = createStackNavigator()
const RegisterStack = createStackNavigator() 
const AboutStack = createStackNavigator()
const SkillStack = createStackNavigator()
const ProjectStack = createStackNavigator()
const AddStack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()


const LoginStackScreen = () => {
    return(
        <LoginStack.Navigator>
            <LoginStack.Screen name="Login" component={Login}/>
            <LoginStack.Screen name="Register" component={RegisterStackScreen}/>
        </LoginStack.Navigator>
    ) 
}

const RegisterStackScreen = () => {
    return(
        <RegisterStack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <RegisterStack.Screen name="Register" component={Register}/>
        </RegisterStack.Navigator>
    )
}
const AboutStackScreen = () => {
    return(
        <AboutStack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <AboutStack.Screen name="About" component={About}/>
        </AboutStack.Navigator>
    )
}

const SkillStackScreen = () => {
    return(
        <SkillStack.Navigator 
            screenOptions={{
                headerShown: false,
            }}>
            <SkillStack.Screen name="Skill" component={Skill}/>
        </SkillStack.Navigator>
    )
}

const ProjectStackScreen = () => {
    return(
        <ProjectStack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <ProjectStack.Screen name="Project" component={Project}/>
        </ProjectStack.Navigator>
    )
}

const AddStackScreen = () => {
    return(
        <AddStack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <AddStack.Screen name="Add" component={Add}/>
        </AddStack.Navigator>
    )
}
const TabScreen = () => {
    return(
        <Tabs.Navigator
            tabBarOptions={{
                activeTintColor: "#0BCAD4",
                style: {
                    backgroundColor: "#fff"
                },
                labelStyle: {
                    fontSize: 12,
                    marginBottom: 10,
                    fontWeight: "bold"
                },
            }}>
            <Tabs.Screen name="Skill" component={SkillStackScreen}/>
            <Tabs.Screen name="Project" component={ProjectStackScreen}/>
            <Tabs.Screen name="Add" component={AddStackScreen}/>
        </Tabs.Navigator>
    )
}

const DrawerScreen = () => {
    return(
    <Drawer.Navigator>
        <Drawer.Screen name="Skill" component={TabScreen}/>
        <Drawer.Screen name="About" component={AboutStackScreen}/>
    </Drawer.Navigator>
    )
}

export default () => {
    return(
        <NavigationContainer>
            <LoginStack.Navigator screenOptions={{
                headerShown: false,
            }}>
                <LoginStack.Screen name="Login" component={Login}/>
                <LoginStack.Screen name="Register" component={RegisterStackScreen} />
                <LoginStack.Screen name="Drawer" component={DrawerScreen}/>
            </LoginStack.Navigator>
        </NavigationContainer>
    )
}