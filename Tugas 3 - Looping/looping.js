// Soal 1. Looping While
console.log("==== Soal 1. ====")
var bil = 0
console.log("LOOPING PERTAMA")
while (bil<20) {
    bil+=2
    console.log(`${bil} - I love coding`)
}

console.log("LOOPING KEDUA")
while (bil>=2) {
    console.log(`${bil} - I will become a mobile developer`)
    bil-=2
}

// Soal 2. Looping menggunakan for
console.log("==== Soal 2. ====")
for (for1 = 1; for1 <= 20; for1++){
    if (for1 % 2 != 0 && for1 % 3 == 0){
        console.log(`${for1} - I Love Coding`)
    }
    else if (for1 % 2 != 0){
        console.log(`${for1} - Santai`)
    }
    else if (for1 % 2 == 0){
        console.log(`${for1} - Berkualitas`)
    }
}

// Soal 3. Membuat Persegi Panjang
console.log("==== Soal 3. ====")
var panjang = 8
var lebar = 4
var hasil = ""
for (i = 0; i<lebar; i++){
    for (j = 0; j<panjang; j++){
        hasil +="#"
    }
    hasil += "\n"
}
console.log(hasil)

// Soal 4. Membuat Tangga
console.log("==== Soal 4. ====")
var tinggi = 7
var alas = 7
var result = ""
for (k = 0; k<tinggi; k++){
    for(l = 0; l<=k; l++){
        result+= "#"
    }
    result += "\n"
}
console.log(result)

// Soal 5. Membuat papan catur
console.log("==== Soal 5. ====")
var sisi = 8
var res = ""
for (x = 0; x<sisi; x++){
    for (y = 0; y<sisi; y++){
        if (x % 2 != 0){
            if (y % 2 != 0){
                res +=" "
            }
            else {
                res +="#"
            }
        } else {
            if (y % 2 == 0){
                res +=" "
            }
            else {
                res +="#"
            }
        }
    }
    res += "\n"
}
console.log(res)


