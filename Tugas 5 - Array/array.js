// Soal 1.
function range (startNum, finishNum){
    var hasil =[]
    if (startNum == null || finishNum == null){
        return -1
    } else {
        if (startNum < finishNum){
            for (i = startNum; i<=finishNum; i++){
                hasil.push(i)
            }
        } else {
            for (i = startNum; i >= finishNum; i--){
                hasil.push(i)
            }
        }
        return hasil
    }
}

console.log("==== Soal 1 ====")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Soal 2.
function rangeWithStep (startNum, finishNum, step){
    var hasil =[]
    if (step == null){
        step = 1
    }
    if (startNum != null && finishNum == null){
        hasil.push(startNum)
    } 
    else {
        if (startNum < finishNum){
            for (i = startNum; i<=finishNum; i+=step){
                hasil.push(i)
            }
        } else {
            for (i = startNum; i >= finishNum; i-=step){
                hasil.push(i)
            }
        }
    }
    return hasil
}
console.log("==== Soal 2 ====")
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

// Soal 3.
function sum(startNum, finishNum, step){
    var nilai = rangeWithStep(startNum, finishNum, step)
    var sum = 0;
    for (i = 0; i < nilai.length; i++){
        sum += nilai[i]
    }
    return sum
}
console.log("==== Soal 3 ====")
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

// Saol 4.
function dataHandling(input){
    var data = ""
    for (i = 0; i<input.length; i++){
        data += `Nomor ID : ${input[i][0]}\n`
        data += `Nama Lengkap : ${input[i][1]}\n`
        data += `TTL : ${input[i][2]} ${input[i][3]}\n`
        data += `Hobi : ${input[i][4]}\n`
        data += "\n"
    }
    return data
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log("==== Soal 4 ====")
console.log(dataHandling(input))

// Soal 5.
function balikKata(kata){
    var kataBaru = "";
    for (i = kata.length-1; i >= 0; i--){
        kataBaru += kata[i]
    }
    return kataBaru
}
console.log("==== Soal 5 ====")
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

// Soal 6.
function dataHandling2(input2){
    console.log("Input Awal :")
    console.log(input2)

    console.log("Splice :")
    input2.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    input2.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(input2)
    
    console.log("Split :")
    var date = input2[3].split("/")
    var bulan = date[1]
    var namaBulan = ""

    switch(bulan) {
        case "01":{
            namaBulan = "Januari"
            break
        }
        case "02":{
            namaBulan = "Februari"
            break
        }
        case "03":{
            namaBulan = "Maret"
            break
        }
        case "04":{
            namaBulan = "April"
            break
        }
        case "05":{
            namaBulan = "Mei"
            break
        }
        case "06":{
            namaBulan = "Juni"
            break
        }
        case "07":{
            namaBulan = "Juli"
            break
        }
        case "08":{
            namaBulan = "Agustus"
            break
        }
        case "09":{
            namaBulan = "September"
            break
        }
        case "10":{
            namaBulan = "Oktober"
            break
        }
        case "11":{
            namaBulan = "November"
            break
        }
        case "12":{
            namaBulan = "Desember"
            break
        }
        default:{
            namaBulan = ""
        }
    }
    console.log(namaBulan)

    console.log("Sort :")
    var dateSort = date.slice()
    dateSort.sort(function (value1, value2) { return value2 - value1 } )
    console.log(dateSort)

    console.log("Join :")
    var fullDate = date.join("-")
    console.log(fullDate)

    console.log("Slice :")
    var nama = input2[1].slice(0, 14)
    console.log(nama)

    return input2
}

console.log("==== Soal 6 ====")
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input2)