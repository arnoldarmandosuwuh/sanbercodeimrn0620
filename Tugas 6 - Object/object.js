// Soal 1. Array to Object
function arrayToObject(array){
    var bio = {}
    var object = {}
    var hasil = []

    var now = new Date()
    var thisYear = now.getFullYear()

    for (var i = 0; i < array.length; i++){
        if(array[i][3] == null || array[i][3] > thisYear){
            object = {
                firstName : array[i][0],
                lastName : array[i][1],
                gender : array[i][2],
                age : "Invalid Birth Year"
            }
        } else {
            object = {
                firstName : array[i][0],
                lastName : array[i][1],
                gender : array[i][2],
                age : thisYear - array[i][3]
            }
        }
        bio[`${array[i][0]} ${array[i][1]}`] = object
    }
    hasil.push(bio)
    
    return hasil
}

console.log("=== Soal 1. Array to Object ===")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log(arrayToObject(people)) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log(arrayToObject(people2))

// Soal 2. Shopping Time
function shoppingTime(memberId, money){
    var barang = [
        ["Sepatu Stacattu", 1500000], 
        ["Baju Zoro", 500000], 
        ["Baju H&N", 250000], 
        ["Sweater Uniklooh", 175000], 
        ["Casing Handphone", 50000]
    ]
    var uang = money
    var barangBelanja = []
    if (memberId == null || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    if (money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    for (var i = 0; i < barang.length; i++){
        if (barang[i][1] <= uang){
            uang -= barang[i][1]
            barangBelanja.push(barang[i][0])
        } else {
            continue
        }
    }
    var barang = {
        memberId: memberId,
        money: money,
        listPurchased: barangBelanja,
        changeMoney: uang
    }
    return barang
}
console.log("=== Soal 2. Shopping Time ===")
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 

// Soal 3. Naik Angkot
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var angkot = []
    for (var i = 0; i < listPenumpang.length; i++){
        var tarif = (rute.indexOf(listPenumpang[i][2]) - rute.indexOf(listPenumpang[i][1])) * 2000
        penumpang = {
            penumpang: listPenumpang[i][0],
            naikDari: listPenumpang[i][1],
            tujuan: listPenumpang[i][2],
            bayar: tarif
        }
        angkot.push(penumpang)
    }
    return angkot
}
console.log("=== Soal 3. Naik Angkot ===")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]