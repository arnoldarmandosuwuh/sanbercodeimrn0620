// Soal 1. Animal Class
class Animal {
    constructor (nama){
        this.nama = nama
        this.kaki = 4
        this.darah_dingin = false
    }
    get name(){
        return this.nama
    }
    get legs(){
        return this.kaki
    }
    get cold_blooded(){
        return this.darah_dingin
    }
}

console.log("=== Soal 1. Animal Class ===")
console.log("--- Release 0 ---")
var sheep = new Animal("shaun"); 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor (nama){
        super(nama)
        this.kaki = 2
        this.yel = "Auooo"
    }
    get legs(){
        return this.kaki
    }
    yell() {
        console.log(this.yel)
    }
}

class Frog extends Animal {
    constructor (nama){
        super(nama)
        this.loncat = "hop hop"
    }
    jump() {
        console.log(this.loncat)
    }
}


console.log("--- Release 1 ---")

var sungokong = new Ape("kera sakti")
sungokong.yell() 

var kodok = new Frog("buduk")
kodok.jump()

// Soal 2. Function to Class
class Clock {
    constructor({ template }){
        this.template = template
        this.timer
    }

    render(){
        var date = new Date()

        var hours = date.getHours()
        if (hours < 10) hours = '0' + hours

        var mins = date.getMinutes()
        if (mins < 10) mins = '0' + mins

        var secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)

        console.log(output)
    }
    
    start() {
        this.render
        this.timer = setInterval(this.render.bind(this), 1000)
    }

    stop() {
        clearInterval(this.timer)
    }
}

console.log("=== Soal 2. Function to Class ===")
var clock = new Clock({template: 'h:m:s'})
clock.start()