var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0
var timeAvailable = 10000

function readBook(books, timeAvailable){
    if (i < books.length){
        readBooks(timeAvailable, books[i], function(check){
            if (check){
                readBook(books, timeAvailable)
            }
        })
        timeAvailable -= books[i].timeSpent
        i++
    }
}
readBook(books, timeAvailable)