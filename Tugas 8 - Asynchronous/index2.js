var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var i = 0
var timeAvailable = 10000

function readBookPromise(books, timeAvailable){
    if (i < books.length){
        readBooksPromise(timeAvailable, books[i])
            .then(function (fulfilled){
                readBookPromise(books, timeAvailable)
            })
            .catch (function (error){
                console.log(error.message)
            })
        timeAvailable -= books[i].timeSpent
        i++
    }
}
readBookPromise(books, timeAvailable)