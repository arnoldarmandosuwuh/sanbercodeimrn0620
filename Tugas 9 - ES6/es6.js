// Soal 1. Mengubah fungsi menjadi fungsi arrow
const golden = () => console.log("this is golden!!")

console.log("=== Soal 1. Mengubah fungsi menjadi fungsi arrow ===")
golden()

// Soal 2. Sederhanakan menjadi Object literal di ES6
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }
}
   
//Driver Code 
console.log("=== Soal 2. Sederhanakan menjadi Object literal di ES6 ===")
newFunction("William", "Imoh").fullName()

// Soal 3. Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation} = newObject
// Driver code
console.log("=== Soal 3. Destructuring ===")
console.log(firstName, lastName, destination, occupation)

// Soal 4. Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log("=== Soal 4. Array Spreading ===")
console.log(combined)

//Soal 5. Template Literals
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log("=== Soal 5. Template Literals ===")
console.log(before) 
